def solve(filename, preamble):
    with open(filename) as file:
        input = [int(x) for x in file.read().split("\n")]

    for idx in range(preamble, len(input)):
        combinations = []
        for x in input[idx - preamble:idx]:
            for y in input[idx - preamble:idx]:
                combinations.append(x + y)
        if input[idx] not in combinations:
            return input[idx]


assert solve("example.txt", 5) == 127

print(solve("input.txt", 25))
