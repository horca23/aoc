def solve(filename, target):
    with open(filename) as file:
        input = [int(x) for x in file.read().split("\n")]

    for a in range(len(input) - 1):
        for b in range(1, len(input)):
            slc = input[a:b]
            if sum(slc) == target:
                mx = max(slc)
                mn = min(slc)
                return mx + mn


assert solve("example.txt", 127) == 62

print(solve("input.txt", 14360655))
