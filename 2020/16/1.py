class Range:
    def __init__(self, line):
        parts = line.split(" ")

        first_range = parts[-3].split("-")
        self.first = range(int(first_range[0]), int(first_range[1]) + 1)

        second_range = parts[-1].split("-")
        self.second = range(int(second_range[0]), int(second_range[1]) + 1)

    def is_valid(self, num):
        return num in self.first or num in self.second


def parse_rules(input):
    return [Range(line) for line in input]

def parse_nearby(input):
    return [map(int, line.split(",")) for line in input]

def solve(filename):
    with open(filename) as file:
        input = file.read().split("\n\n")
    
    rules = parse_rules(input[0].splitlines())
    nearby = parse_nearby(input[2].splitlines()[1:])

    err_rate = 0
    for n in nearby:
        for t in n:
            if not any(r.is_valid(t) for r in rules):
                err_rate += t
    return err_rate

example = solve("example.txt")
print(example)
assert example == 71
print(solve("input.txt"))