class Memory(dict):
    def set_mask(self, mask):
        self.mask = mask

    def __setitem__(self, index, value):
        return super().__setitem__(index, self._apply_mask(value))

    def _apply_mask(self, value):
        b = bin(value)[2:].zfill(36)
        for i, m in enumerate(self.mask):
            if m == "X":
                continue
            elif m != b[i]:
                b = b[:i] + m + b[i+1:]
        return int(b, 2)


def solve(filename):
    mem = Memory()
    with open(filename) as file:
        input = file.read().splitlines()

    for line in input:
        if line[1] == "a":
            mem.set_mask(line[7:])
        else:
            exec(line)

    return sum(mem.values())

example = solve("example.txt")
print(example)
assert example == 165
print(solve("input.txt"))
