from itertools import product

class Memory(dict):
    def set_mask(self, mask):
        self.mask = mask

    def __setitem__(self, index, value):
        target_addrs = self._get_target_addrs(index)
        for addr in target_addrs:
            super().__setitem__(addr, value)

    def _get_target_addrs(self, starting_addr):
        starting_addr = self._merge_with_mask(starting_addr)
        addresses = []

        to_replace = starting_addr.count("X")
        starting_addr = starting_addr.replace("X", "{}")

        for n in range(2**to_replace):
            n = bin(n)[2:].zfill(to_replace)
            addresses.append(starting_addr.format(*n))

        return map(lambda x: int(x, 2), addresses)

    def _merge_with_mask(self, index):
        b = bin(index)[2:].zfill(36)
        for i, m in enumerate(self.mask):
            if m != "0" and b[i] != m:
                b = b[:i] + m + b[i+1:]

        return b

def solve(filename):
    mem = Memory()
    with open(filename) as file:
        input = file.read().splitlines()

    for line in input:
        if line[1] == "a":
            mem.set_mask(line[7:])
        else:
            exec(line)

    return sum(mem.values())

example = solve("example2.txt")
print(example)
assert example == 208
print(solve("input.txt"))
