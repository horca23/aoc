with open("input.txt") as file:
    input = [int(line) for line in file]

for i in input:
    for j in input:
        for k in input:
            if i + j + k == 2020:
                print("{}, {}, {}".format(i, j, k))
                print(i * j * k)