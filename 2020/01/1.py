with open("input.txt") as file:
    input = [int(line) for line in file]

for i in input:
    for j in input:
        if i + j == 2020:
            print("{}, {}".format(i, j))
            print(i * j)