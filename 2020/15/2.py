def solve(numbers):
    # Van Ack's sequence
    last = {}
    for i in range(len(numbers)):
        last[numbers[i]] = i
    curr = 0
    for i in range(len(numbers), 30000000):
        prev = curr
        if curr not in last.keys():
            curr = 0
        else:
            curr = i - last[curr]
        last[prev] = i
    return prev

print(solve([7,12,1,0,16,2]))