def solve(numbers):
    # Van Ack's sequence
    last = {}
    for i in range(len(numbers)):
        last[numbers[i]] = i
    curr = 0
    for i in range(len(numbers), 2020):
        prev = curr
        if curr not in last.keys():
            curr = 0
        else:
            curr = i - last[curr]
        last[prev] = i
    return prev

assert solve([0,3,6]) == 436
assert solve([1,3,2]) == 1
assert solve([2,1,3]) == 10
assert solve([1,2,3]) == 27
assert solve([2,3,1]) == 78
assert solve([3,2,1]) == 438
assert solve([3,1,2]) == 1836

print(solve([7,12,1,0,16,2]))