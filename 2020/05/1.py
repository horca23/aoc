with open("input.txt") as file:
    input = [line.rstrip() for line in file]


def decode_seat(to_decode):
    binary = to_decode \
        .replace("F", "0") \
        .replace("B", "1") \
        .replace("R", "1") \
        .replace("L", "0")
    return int(binary, 2)


highest = max(decode_seat(line) for line in input)
print(highest)
