with open("input.txt") as file:
    grid = [line.rstrip() for line in file]

grid_width = len(grid[0])

def move_right(x, distance):
    return (x + distance) % grid_width

def count_trees_hit(slope_x, slope_y):
    x = 0
    trees_hit = 0
    for y in grid[::slope_y]:
        if y[x] == "#":
            trees_hit += 1
        x = move_right(x, slope_x)
    return trees_hit

trees_hit_total = 1
for slope_x, slope_y in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
    trees_hit_total *= count_trees_hit(slope_x, slope_y)

print(trees_hit_total)