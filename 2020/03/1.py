with open("input.txt") as file:
    grid = [line.rstrip() for line in file]

grid_width = len(grid[0])

def move_right(x):
    return (x + 3) % grid_width

x = 0
trees_hit = 0

for y in grid:
    if y[x] == "#":
        trees_hit += 1
    x = move_right(x)

print(trees_hit)