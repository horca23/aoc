from functools import reduce
def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod
 
 
 
def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1

def solve(filename):
    with open(filename) as file:
        input = file.read().splitlines()
        ids = [id for id in input[1].split(",")]

    n = []
    a = []
    for i in range(len(ids)):
        id = ids[i]
        if id == "x":
            continue
        # e.g. 17, x, 13, 19
        # x ≡ 0(mod 17)
        # x ≡ -2(mod 13)
        # x ≡ -3(mod 19)
        n.append(int(id))
        a.append(-i)

    return chinese_remainder(n, a)

example = solve("example.txt")
print(example)
assert example == 1068781

example2 = solve("example2.txt")
print(example2)
assert example2 == 3417

print(solve("input.txt"))
