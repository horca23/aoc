def solve(filename):
    with open(filename) as file:
        input = file.read().splitlines()
        e = int(input[0])
        ids = [int(id) for id in input[1].split(",") if id != "x"]

    og_e = e
    while True:
        for id in ids:
            if e % id == 0:
                return (e - og_e) * id
        e += 1

    return -1

example = solve("example.txt")
print(example)
assert example == 295
print(solve("input.txt"))
