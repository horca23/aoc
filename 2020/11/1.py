def deep_copy(rows):
    rows_copy = []
    for row in rows:
        rows_copy.append(row[:])
    return rows_copy

def is_inside(row, seat, rows):
    return row in range(len(rows)) and seat in range(len(rows[0]))
    
def count_occupied_neighbors(row, seat, rows):
    occupied = 0
    directions = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1)
    ]

    for vertical, horizontal in directions:
        next_row = row + vertical
        next_seat = seat + horizontal

        if not is_inside(next_row, next_seat, rows):
            continue

        if rows[next_row][next_seat] == "#":
            occupied += 1

    return occupied

def count_occupied_seats(rows):
    occupied = 0
    for row in rows:
        for seat in row:
            if seat == "#":
                occupied += 1
    return occupied

def solve(filename):
    with open(filename) as file:
        rows = [list(row) for row in file.read().split("\n")]

    while True:
        rows_next = deep_copy(rows)
        state_updated = False

        for row in range(len(rows)):
            for seat in range(len(rows[row])):
                if rows[row][seat] == ".":
                    continue

                occupied_neighbors = count_occupied_neighbors(row, seat, rows)
                if rows[row][seat] == "L" and occupied_neighbors == 0:
                    rows_next[row][seat] = "#"
                    state_updated = True
                elif rows[row][seat] == "#" and occupied_neighbors >= 4:
                    rows_next[row][seat] = "L"
                    state_updated = True

        if state_updated:
            rows = rows_next
        else:
            break
            
    return count_occupied_seats(rows)

example = solve("example.txt")
print(example)
assert example == 37
print(solve("input.txt"))
