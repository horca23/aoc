def solve(filename):
    with open(filename) as file:
        input = [0] + sorted([int(x) for x in file.read().split("\n")])
        input.append(max(input) + 3)

    diff_one = 0
    diff_three = 0

    for idx in range(len(input) - 1):
        curr, next = input[idx], input[idx + 1]
        if next - curr == 1:
            diff_one += 1
        elif next - curr == 3:
            diff_three += 1

    return diff_one * diff_three


example = solve("example.txt")
print(example)
assert example == 35
print(solve("input.txt"))
