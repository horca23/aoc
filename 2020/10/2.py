def get_possible_next_indices(idx, input):
    possible_next_indices = []
    for x in range(idx + 1, len(input)):
        if input[x] < input[idx] + 4:
            possible_next_indices.append(x)
    return possible_next_indices


def calc(idx, input):
    if idx == len(input) - 1:
        return 1
    else:
        acc = 0
        possible_next_indices = get_possible_next_indices(idx, input)
        for possible_next_index in possible_next_indices:
            acc += calc(possible_next_index, input)
        return acc


def split(lst):
    first = lst[0:len(lst)//2]
    second = lst[len(lst)//2:]
    first.append(second[0])
    return first, second


def solve(filename):
    with open(filename) as file:
        input = [int(x) for x in file.read().split("\n")]
        input.append(0)
        input.append(max(input) + 3)
        input.sort()

    first, second = split(input)

    return calc(0, first) * calc(0, second)


example = solve("example.txt")
print(example)
assert example == 8
print(solve("input.txt"))
