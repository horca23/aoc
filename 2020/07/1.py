with open("input.txt") as file:
    input = [line.rstrip() for line in file]

rules = {}

for line in input:  # 'clear purple bags contain 5 faded indigo bags, 3 muted purple bags.'
    parts = [value.rstrip() for value in line.split("bag")] # ['clear purple', 's contain 5 faded indigo', 's, 3 muted purple', 's.']
    key = parts[0]  # 'clear purple'
    individual_rules = [value.split() for value in parts[1:-1]] # [['s', 'contain', '5', 'faded', 'indigo'], ['s,', '3', 'muted', 'purple']]
    for individual_rule in individual_rules:
        rule = individual_rule[-2] + " " + individual_rule[-1]
        rules[key] = rules.get(key, []) + [rule]

def can_contain_shiny_gold_recursive(rule_key, visited = []):
    if rule_key == "no other":
        return False
    elif rule_key == "shiny gold":
        return True
    else:
        visited.append(key)
        for rule in rules[rule_key]:
            if can_contain_shiny_gold_recursive(rule, visited):
                return True

can_contain_shiny_gold = set()
for rule in rules:
    if rule == "shiny gold":
        continue
    if can_contain_shiny_gold_recursive(rule):
        can_contain_shiny_gold.add(rule)

print(len(can_contain_shiny_gold))
