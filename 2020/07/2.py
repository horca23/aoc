with open("input.txt") as file:
    input = [line.rstrip() for line in file]

rules = {}
sum = 0

for line in input:  # 'clear purple bags contain 5 faded indigo bags, 3 muted purple bags.'
    parts = [value.rstrip() for value in line.split("bag")] # ['clear purple', 's contain 5 faded indigo', 's, 3 muted purple', 's.']
    key = parts[0]  # 'clear purple'
    individual_rules = [value.split() for value in parts[1:-1]] # [['s', 'contain', '5', 'faded', 'indigo'], ['s,', '3', 'muted', 'purple']]
    for individual_rule in individual_rules:
        if "no" in individual_rule and "other" in individual_rule:
            rule = []
        else:
            bag_color = individual_rule[-2] + " " + individual_rule[-1]
            count = int(individual_rule[-3])
            rule = [(bag_color, count)]
        rules[key] = rules.get(key, []) + rule

def count_subnodes(root):
    subnodes = rules[root]
    sum_of_subnodes = 0
    for color, count in subnodes:
        sum_of_subnodes += count + count * count_subnodes(color)
    return sum_of_subnodes


print(count_subnodes("shiny gold"))