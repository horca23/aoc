import os
import sys
import concurrent.futures
import nltk

def sentece_matches_grammar(i, line, parser):
    print("Processing line #{}".format(i))
    chars = [char for char in line]
    for _ in parser.parse(chars):
        return 1
    return 0

def solve(filename, multithread=False):
    with open(filename) as file:
        grammar, input = file.read().split("\n\n")

    grammar = grammar.replace(":", " ->")
    grammar = grammar.replace("8 -> 42", "8 -> 42 | 42 8").replace("11 -> 42 31", "11 -> 42 31 | 42 11 31")
    grammar = "start -> 0\n" + grammar
    grammar = nltk.CFG.fromstring(grammar)
    parser = nltk.RecursiveDescentParser(grammar)

    input = input.splitlines()

    if multithread:
        with concurrent.futures.ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
            futures = [executor.submit(sentece_matches_grammar, i, sentence, parser) for i, sentence in enumerate(input, start=1)]
            return sum(future.result() for future in concurrent.futures.as_completed(futures))
    else:
        return sum(sentece_matches_grammar(i, sentence, parser) for i, sentence in enumerate(input, start=1))

try:
    multithread = sys.argv[1]
except:
    multithread = None

example = solve("example2.txt", multithread)
print(example)
assert example == 12
print(solve("input.txt", multithread))