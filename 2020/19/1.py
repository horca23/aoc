import nltk

def solve(filename):
    with open(filename) as file:
        grammar, input = file.read().split("\n\n")

    grammar = grammar.replace(":", " ->")
    grammar = "start -> 0\n" + grammar
    grammar = nltk.CFG.fromstring(grammar)
    parser = nltk.RecursiveDescentParser(grammar)
    valid = 0

    for line in input.splitlines():
        chars = [char for char in line]
        for _ in parser.parse(chars):
            valid += 1

    return valid


example = solve("example.txt")
print(example)
assert example == 2
print(solve("input.txt"))