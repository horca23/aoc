with open("input.txt") as file:
    input = [line.rstrip() for line in file]

def get_range(range):
    parts = range.split("-")
    return int(parts[0]), int(parts[1])

def parse_line(line):
    parts = line.split()
    min, max = get_range(parts[0])
    letter = parts[1].replace(":", "")
    passwd = parts[2]
    return (min, max), letter, passwd


def is_passwd_valid(line):
    range, letter, passwd = parse_line(line)
    letters_found = passwd.count(letter)
    return range[0] <= letters_found <= range[1]
    
counter = 0

for line in input:
    if is_passwd_valid(line):
        counter += 1

print(counter)