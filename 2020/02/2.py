with open("input.txt") as file:
    input = [line.rstrip() for line in file]

def get_pos(range):
    parts = range.split("-")
    return int(parts[0]) - 1, int(parts[1]) - 1

def parse_line(line):
    parts = line.split()
    first_pos, second_pos = get_pos(parts[0])
    letter = parts[1].replace(":", "")
    passwd = parts[2]
    return (first_pos, second_pos), letter, passwd


def is_passwd_valid(line):
    pos, letter, passwd = parse_line(line)
    first_pos_valid = passwd[pos[0]] == letter
    last_pos_valid = passwd[pos[1]] == letter
    return first_pos_valid ^ last_pos_valid
    
counter = 0

for line in input:
    if is_passwd_valid(line):
        counter += 1

print(counter)