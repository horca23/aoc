class Passport:
    def __init__(self, passport_str):
        self.fields = {}
        for field in passport_str.replace("\n", " ").split():
            key, value = field.split(":")
            self.fields[key] = value

    def is_valid(self):
        return (len(self.fields) == 8) or (len(self.fields) == 7 and "cid" not in self.fields)


with open("input.txt") as file:
    input = map(Passport, file.read().split("\n\n"))

valid_passports = len([passport for passport in input if passport.is_valid()])

print(valid_passports)
