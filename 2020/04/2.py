import string


class Passport:
    def _validate_hgt(x):
        valid_cm = x[-2:] == "cm" and x[:3].isdigit() and 150 <= int(x[:3]) <= 193
        valid_in = x[-2:] == "in" and x[:2].isdigit() and 59 <= int(x[:2]) <= 76
        return valid_cm or valid_in

    def _validate_hcl(x):
        starts_with_tag = x.startswith("#")
        contains_six_number = len(x[1:]) == 6
        is_hexa_number = all(char in string.hexdigits for char in x[1:])
        return starts_with_tag and contains_six_number and is_hexa_number

    _validations = {
        "byr": lambda x: x.isdigit() and 1920 <= int(x) <= 2002,
        "iyr": lambda x: x.isdigit() and 2010 <= int(x) <= 2020,
        "eyr": lambda x: x.isdigit() and 2020 <= int(x) <= 2030,
        "hgt": _validate_hgt,
        "hcl": _validate_hcl,
        "ecl": lambda x: x in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
        "pid": lambda x: len(x) == 9 and x.isdigit(),
        "cid": lambda _: True,
    }

    def __init__(self, passport_str):
        self.fields = {}
        for field in passport_str.replace("\n", " ").split():
            key, value = field.split(":")
            self.fields[key] = value

    def is_valid(self):
        if (len(self.fields) == 8) or (len(self.fields) == 7 and "cid" not in self.fields):
            return all([Passport._validations[field_key](field_value) for field_key, field_value in self.fields.items()])
        else:
            return False


with open("input.txt") as file:
    input = map(Passport, file.read().split("\n\n"))


valid_passports = len([passport for passport in input if passport.is_valid()])

print(valid_passports)
