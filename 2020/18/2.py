import re

class MyInt(int):
    def __mul__(self, other):
        return MyInt(int.__mul__(self, other))

    def __pow__(self, other):
        return MyInt(int.__add__(self, other))


def process_line(line):
    line = re.sub(r"(\d+)", r"MyInt(\1)", line)
    line = line.replace("+", "**")
    return eval(line)

def solve(input):
    return sum(process_line(line) for line in input)

example = solve([
    "2 * 3 + (4 * 5)",
    "5 + (8 * 3 + 9 + 3 * 4 * 3)",
    "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
    "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"
])
print(example)
assert example == 46 + 1445 + 669060 + 23340

with open("input.txt") as file:
    input = file.read().splitlines()
print(solve(input))