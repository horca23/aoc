class Instruction:
    def __init__(self, op, arg, invocations=0):
        self.op = op
        self.arg = arg
        self.invocations = invocations

    @classmethod
    def from_input_line(cls, line_str):
        op, arg = line_str.split()
        return cls(op, int(arg))

    def copy(self):
        return Instruction(self.op, self.arg, self.invocations)


with open("input.txt") as file:
    input = [Instruction.from_input_line(line.rstrip()) for line in file]


def run(program):
    global_acc = 0
    idx = 0

    while idx < len(program):
        instruction = program[idx]
        if instruction.invocations > 0:
            return -1
        op = instruction.op
        if op == "acc":
            global_acc += instruction.arg
            idx += 1
        elif op == "nop":
            idx += 1
        elif op == "jmp":
            idx += instruction.arg
        instruction.invocations += 1
    
    return global_acc


for idx in range(len(input)):
    if input[idx].op == "acc":
        continue

    copy = [i.copy() for i in input]
    instruction = copy[idx]
    if instruction.op == "jmp":
        instruction.op = "nop"
    else:
        instruction.op = "jmp"
    
    result = run(copy)
    if result != -1:
        print(result)

