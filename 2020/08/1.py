class Instruction:
    def __init__(self, line_str):
        op, arg = line_str.split()
        self.op = op
        self.arg = int(arg)
        self.invocations = 0

def solve(input):
    input = [Instruction(line.rstrip()) for line in input.split("\n")]
    global_acc = 0
    idx = 0

    while True:
        instruction = input[idx]
        if instruction.invocations == 1:
            break
        op = instruction.op
        if op == "acc":
            global_acc += instruction.arg
            idx += 1
        elif op == "nop":
            idx += 1
        elif op == "jmp":
            idx += instruction.arg
        instruction.invocations += 1

    return global_acc


with open("input.txt") as file:
    input = file.read()
print(solve(input))
