def solve(filename):
    with open(filename) as file:
        input = file.read().splitlines()

    direction = "E"
    directions = {
        "N": (0, 1, "W", "E"),
        "E": (1, 0, "N", "S"),
        "S": (0, -1, "E", "W"),
        "W": (-1, 0, "S", "N")
    }
    x, y = 0, 0

    for instruction in input:
        move = instruction[0]
        distance = int(instruction[1:])

        if move == "N":
            y = y + distance
        elif move == "E":
            x = x + distance
        elif move == "S":
            y = y - distance
        elif move == "W":
            x = x - distance
        elif move == "F":
           d_x, d_y, _, _ = directions[direction]
           x = x + distance * d_x
           y = y + distance * d_y
        elif move == "R":
            turns = distance // 90
            for _ in range(turns):
                _, _, _, to_right = directions[direction]
                direction = to_right
        elif move == "L":
            turns = distance // 90
            for _ in range(turns):
                _, _, to_left, _ = directions[direction]
                direction = to_left

    return abs(x) + abs(y)

example = solve("example.txt")
print(example)
assert example == 25
print(solve("input.txt"))
