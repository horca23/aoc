import math

def rotate(x, y, degrees):
    radians = math.radians(degrees)
    # PGRF FTW
    sin_r = math.sin(radians)
    cos_r = math.cos(radians)
    rotated_x = x * cos_r + y * sin_r 
    rotated_y = -x * sin_r + y * cos_r 

    return int(round(rotated_x)), int(round(rotated_y))

def solve(filename):
    with open(filename) as file:
        input = file.read().splitlines()

    x, y = 0, 0
    w_x, w_y = 10, 1

    for instruction in input:
        move = instruction[0]
        distance = int(instruction[1:])

        if move == "N":
            w_y = w_y + distance
        elif move == "E":
            w_x = w_x + distance
        elif move == "S":
            w_y = w_y - distance
        elif move == "W":
            w_x = w_x - distance
        elif move == "F":
            x = x + distance * w_x
            y = y + distance * w_y
        elif move == "R":
            w_x, w_y = rotate(w_x, w_y, distance)
        elif move == "L":
            w_x, w_y = rotate(w_x, w_y, -distance)

    return abs(x) + abs(y)

example = solve("example.txt")
print(example)
assert example == 286
print(solve("input.txt"))
