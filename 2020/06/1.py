class Group:
    def __init__(self, answers_str):
        self.answers = answers_str.replace("\n", "")

    def count_unique_answers(self):
        return len(set(self.answers))


with open("input.txt") as file:
    input = map(Group, file.read().split("\n\n"))


print(sum(group.count_unique_answers() for group in input))
