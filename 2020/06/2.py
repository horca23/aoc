class Group:
    def __init__(self, answers_str):
        self.answers = answers_str.split("\n")

    def count_questions_with_yes_from_everyone(self):
        return len(set.intersection(*map(set, self.answers)))


with open("input.txt") as file:
    input = map(Group, file.read().split("\n\n"))


print(sum(group.count_questions_with_yes_from_everyone() for group in input))
