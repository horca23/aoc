class Space:
    def __init__(self, active_cubes):
        self.active_cubes = active_cubes
    
    def step(self):
        self.active_cubes = self._get_next_state()

    def count_active_cubes(self):
        return len(self.active_cubes)

    def _get_next_state(self):
        next_state = set()
        min_x = max_x = min_y = max_y = min_z = max_z = 0
        for cube in self.active_cubes:
            if cube[0] < min_x: min_x = cube[0]
            if cube[0] > max_x: max_x = cube[0]
            if cube[1] < min_y: min_y = cube[1]
            if cube[1] > max_y: max_y = cube[1]
            if cube[2] < min_z: min_z = cube[2]
            if cube[2] > max_z: max_z = cube[2]
        
        for x in range(min_x - 1, max_x + 2):
            for y in range(min_y - 1, max_y + 2):
                for z in range(min_z - 1, max_z + 2):
                    self._update_state(x, y, z, next_state)
        
        return next_state

    def _update_state(self, x, y, z, next_state):
        active = (x, y, z) in self.active_cubes
        neighbors = self._count_neighbors(x, y, z)

        if active and 2 <= neighbors <= 3:
            next_state.add((x, y, z))
        elif not active and neighbors == 3:
            next_state.add((x, y, z))

    def _count_neighbors(self, x, y, z):
        total = 0
        for dx in range(x-1, x+2):
            for dy in range(y-1, y+2):
                for dz in range(z-1, z+2):
                    if dx == x and dy == y and dz ==z:
                        continue
                    if (dx, dy, dz) in self.active_cubes:
                        total += 1
        return total
    

def solve(input):
    cubes = set()
    for y, line in enumerate(input.splitlines()):
        for x, char in enumerate(line):
            if char == "#":
                cubes.add((x, y, 0))

    space = Space(cubes)
    for _ in range(6):
        space.step()

    return space.count_active_cubes()

example = """.#.
..#
###"""
example_result = solve(example)
print(example_result)
assert example_result == 112

input = """##..####
.###....
#.###.##
#....#..
...#..#.
#.#...##
..#.#.#.
.##...#."""
print(solve(input))